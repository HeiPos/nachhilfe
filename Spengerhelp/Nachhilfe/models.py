from django.db import models

#from localflavor.forms gibts phonenumbers

from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin


# Create your models here.

class ProfilManager(BaseUserManager):

    def create_user(self,username,password,nachname,emailadress,vorname):
        profil=self.model()
        profil.username=username
        profil.set_password(password)
        profil.nachname=nachname
        profil.vorname=vorname
        profil.emailadress=emailadress
        profil.save(using=self._db)
        return profil

    def create_superuser(self,username,password,nachname,emailadress,vorname):

        profil=self.create_user(username,password,nachname,emailadress,vorname)
        profil.nachname=nachname
        profil.vorname=vorname
        profil.emailadress=emailadress
        profil.is_active=True
        profil.is_superuser=True
        profil.save(using=self._db)
        return profil


class Profil(AbstractBaseUser,PermissionsMixin):

    username=models.CharField(max_length=250,unique=True)
    vorname=models.CharField(max_length=250)
    nachname=models.CharField(max_length=250)
    geburtsdatum=models.DateField(null=True)#hier kann man noch ein defaultwert angeben
    begruessungstext=models.TextField(max_length=1000,null=True)#florian langeder vermeiden
    emailadress=models.EmailField(unique=True)
    telefonnummer=models.CharField(max_length=250,null=True)
    USERNAME_FIELD="username"
    REQUIRED_FIELDS = ["vorname","nachname","emailadress"]
    objects=ProfilManager()

    def get_full_name(self):
        return self.vorname+" "+self.nachname

class Fach(models.Model):

    bezeichnung=models.CharField(max_length=250)
    jahrgang=models.IntegerField()#HIer sollte noch 1-5 geben minimm ist 1

class Angebot(models.Model):

    user=models.ForeignKey(Profil)
    stundensatz=models.FloatField()
    fach=models.ForeignKey(Fach)
    class Meta:
        unique_together=("user","fach")


























