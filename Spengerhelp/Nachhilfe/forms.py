__author__ = 'Samir'
from django.contrib.auth.forms import UserCreationForm
from django import forms
from models import Profil


class Registrationform(UserCreationForm):

    username= forms.CharField(required=True)
    vorname = forms.CharField(required=True)
    nachname = forms.CharField(required=True)

    class Meta:
        model=Profil
        fields=("vorname","nachname","username","password","password")



    def save(self, commit=True):
        user=super(Registrationform,commit=False)
        user.email=self.cleaned_data["username"]+"@spengergasse.at"
        user.vorname=self.cleaned_data["vorname"]
        user.nachname=self.cleaned_data["nachname"]
        user.username=self.cleaned_data["username"]
        if commit==True:
            user.save()#test
        return user
