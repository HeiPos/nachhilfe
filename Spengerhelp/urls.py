from django.conf.urls import patterns, include, url
from django.contrib.auth.forms import UserCreationForm
from django.views.generic.edit import CreateView, FormView

from django.contrib import admin
from Nachhilfe.forms import Registrationform

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Spengerhelp.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/', include("django.contrib.auth.urls")),
    url(r'^registration/',FormView.as_view(form_class=Registrationform,success_url="/",template_name="registration.html"))




)
